# README #

## What is this repository for? ##

* This repository provides the demo of the digitalGlance() function
* Version 1.0

### What is the digitalGlance() function ?

The digitalGlance() is a function for your Arduino sketch which allows to quickly read the state of a pin as INPUT while is is normally used as OUTPUT. With proper setup and connection, you can use a LED and a button on the same input/output pin.

## How do I get set up? ##

* Summary of set up

    * Connect digital pin 8 to +5V through a 270 ohm resistor and a push button. 
    * Then connect the same pin to GND through another 270 ohm resistor and a LED

* How to run tests
    * Upload the demo to your Arduino. 
    * The LED should blink at 0.5 Hz.
    * Open the serial monitor
    * Watch the BTN value change as you push the button, the LED still blinks normally


[Using Markdown](https://bitbucket.org/tutorials/markdowndemo)