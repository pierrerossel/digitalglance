// Quickly read often a digital IO while also using it mainly as 
// an output.
// This sketch improves read_briefly by providing the digitalGlance() function.
//
// Pierre Rossel 24.03.2014 Initial version
// Pierre Rossel 18.06.2014 Add digitalGlance()

#define LED 8

boolean led = 0;
boolean btn = 0;
unsigned long nextChange = 0;

void setup() {
  Serial.begin(9600);   
}

void loop () {

  // toggle the led value every second
  unsigned long ms = millis();
  if (ms > nextChange) {
    nextChange = ms + 1000;

    led = !led;

    // Write before changing mode otherwise output goes to 0 before we write the value
    // Final value is not set faster, but we avoid unnecessary low level
    digitalWrite(LED, led);
    pinMode(LED, OUTPUT);
  }

  // Quick read, takes about 12 microseconds, then restore OUTPUT mode
  btn = digitalGlance(LED);

  Serial.print("LED: ");
  Serial.print(led);
  Serial.print("\t");

  Serial.print("BTN: ");
  Serial.println(btn);

  delay(100);
}


void writeLed(boolean val) {
  led = val;
  digitalWrite(LED, val);
  pinMode(LED, OUTPUT);
}

