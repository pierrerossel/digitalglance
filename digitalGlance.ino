
// Briefly switches the digital pin to INPUT mode to read its value,
// then restore the previous mode.
// Most useful when you want to use the pin as OUTPUT but still 
// see what you have as input from times to times
boolean digitalGlance(uint8_t pin) {
  
  // save current output and mode register
           uint8_t port   = digitalPinToPort(pin);
  volatile uint8_t *pReg  = portModeRegister(port);
           uint8_t oldReg = *pReg;
  volatile uint8_t *pOut  = portOutputRegister(port);
           uint8_t oldOut = *pOut;
  
  // Quick read, takes about 12 microseconds
  pinMode(pin, INPUT);
  boolean val = digitalRead(pin);
  
  // Restore previous output and mode
  *pOut = oldOut;
  *pReg = oldReg;
  
  return val;
}
